function User(userdata) {
    this._id = userdata._id
    this.username = userdata.username
    this.password = userdata.password
    this.kode_user = userdata.kode_user
    this.level = userdata.level
}

User.prototype.getData = function () {
    return {
        _id: this._id,
        username: this.username,
        password: this.password,
        kode_user: this.kode_user,
        level: this.level
    }
}

module.exports = User