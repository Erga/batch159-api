function Menu(menudata) {
    this._id = menudata._id
    this.name = menudata.name
    this.kode_menu = menudata.kode_menu
    this.price = menudata.price
    this.kode_category = menudata.kode_category
}

Menu.prototype.getData = function () {
    return {
        _id: this._id,
        name: this.name,
        kode_menu: this.kode_menu,
        price: this.price,
        kode_category: this.kode_category
    }
}

module.exports = Menu