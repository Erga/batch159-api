const jwt = require('jsonwebtoken')
const authconfig = require('../configs/auth.config.json')
const ResponseHelper = require('../helpers/responsehelpers')
const authmiddleware = {
    checktoken: (req, res, next) => {
        console.log(req.headers.authorization)
        if (!req.headers.authorization) {
            ResponseHelper.sendResponse(res, 403, 'Failed')
        } else {
            let token = req.headers.authorization
            jwt.verify(token, authconfig.secretkey, (err, decoded) => {
                if (decoded == undefined) {
                    ResponseHelper.sendResponse(res, 403, 'Failed')
                } else {
                    req.userdata = decoded
                    next()
                }
            })
        }
    }
}
module.exports = authmiddleware