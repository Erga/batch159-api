const DB = require('../models/db')
const ObjectID = require('mongodb').ObjectID
const ResponseHelper = require('../helpers/responsehelpers')
const Category = require('../models/categorymodel')
const MiscHelper = require('../helpers/miscHelper')
const db = DB.getConnection()

const CategoryControllers = {
    readAllCategory: (req,res, next) => {
        db.collection('categorys').find({}).toArray((err, docs) => {
            let categorys = docs.map((ele) => {
                return new Category(ele)
            })
            ResponseHelper.sendResponse(res, 200, docs)
        })
    },
    
    // Membuat Category Baru
    createCategory: (req, res, next) => {
        console.log(req.body)

        let data = req.body
        data.kode_category = MiscHelper.createRandomNumber()

        let categoryObject = new Category(data)
        db.collection('categorys').insert(categoryObject, (err, doc) => {
            ResponseHelper.sendResponse(res, 200, doc)
        })
    },

    // Mengubah Category
    updateCategory: (req, res, next) => {
        let data = req.body
        data._id = new ObjectID(req.params.categoryid)
        let categoryObject = new Category(data)
        db.collection('categorys').update(
            {_id: categoryObject._id},
            categoryObject,
            (err, doc) => {
                ResponseHelper.sendResponse(res, 200, doc)
            })
    },

    // Menghapus Category
    deleteCategory: (req, res, next) => {
        let categoryid = new ObjectID(req.params.categoryid)
        db.collection('categorys').remove(
            {_id: categoryid},
            (err, doc) => {
                ResponseHelper.sendResponse(res, 200, doc)
            })    
    }
}

module.exports = CategoryControllers