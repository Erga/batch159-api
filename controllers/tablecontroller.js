const DB = require('../models/db')
const ObjectID = require('mongodb').ObjectID
const ResponseHelper = require('../helpers/responsehelpers')
const Table = require('../models/tablemodel')
const MiscHelper = require('../helpers/miscHelper')
const db = DB.getConnection()

const TableControllers = {
    // Membaca Semua Table
    readAllTable: (req,res, next) => {
        db.collection('tables').find({}).toArray((err, docs) => {
            let tables = docs.map((ele) => {
                return new Table(ele)
            })
            ResponseHelper.sendResponse(res, 200, docs)
        })
    },

    readOneById: (req, res, next) => {
        db.collection('tables').findOne({
            _id: new ObjectID(req.params.tableid)
        }, (err, doc) => {
            ResponseHelper.sendResponse(res, 200, new Table(doc))
        })
    },
    
    // Membuat Table Baru
    createTable: (req, res, next) => {
        console.log(req.body)
        
        let data = req.body
        data.kode_table = MiscHelper.createRandomNumber()

        let tableObject = new Table(data)
        db.collection('tables').insert(tableObject, (err, doc) => {
            ResponseHelper.sendResponse(res, 200, doc)
        })
    },

    // Mengubah Table
    updateTable: (req, res, next) => {
        let data = req.body
        data._id = new ObjectID(req.params.tableid)
        let tableObject = new Table(data)
        db.collection('tables').update(
            {_id: tableObject._id},
            tableObject,
            (err, doc) => {
                ResponseHelper.sendResponse(res, 200, doc)
            })
    },

    // Menghapus Table
    deleteTable: (req, res, next) => {
        let tableid = new ObjectID(req.params.tableid)
        db.collection('tables').remove(
            {_id: tableid},
            (err, doc) => {
                ResponseHelper.sendResponse(res, 200, doc)
            })    
    }
}

module.exports = TableControllers