const DB = require('../models/db')
const ObjectID = require('mongodb').ObjectID
const ResponseHelper = require('../helpers/responsehelpers')
const User = require('../models/usermodel')
const MiscHelper = require('../helpers/miscHelper')
const db = DB.getConnection()
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const authconfig = require('../configs/auth.config.json')

const AuthController = {
    loginHandler: (req, res, next) => {
        if (!req.body.username || !req.body.password) {
            ResponseHelper.sendResponse(res, 404, 'user not found')
        } else {
            db.collection('users').findOne(
            {"username": req.body.username}, 
            (err, doc) => {
                if (doc != null) {
                    let match = bcrypt.compareSync(req.body.password, doc.password);
                    if (match) {
                        delete doc.password
                        let token = jwt.sign(doc, authconfig.secretkey)
                        let data = {
                            userdata: doc,
                            token: token
                        }
                        ResponseHelper.sendResponse(res, 200, data)
                    } else {
                        ResponseHelper.sendResponse(res, 404, 'wrong password')  
                    }
                } else {
                    ResponseHelper.sendResponse(res, 404, 'user not found')  
                }
            }) 
        }
    },

    registerHandler: (req, res, next) => {
        if (!req.body.username || !req.body.password) {
            ResponseHelper.sendResponse(res, 404, 'username & password are required')
        } else {
            let data = req.body
            data.level = 2  
            data.kode_user = MiscHelper.createRandomNumber()
            
            // Encrypt Password User
            let salt = bcrypt.genSaltSync(10);
            let hash = bcrypt.hashSync(data.password, salt);
            data.password = hash

            let userObject = new User(data)
            db.collection('users').insert(userObject, (err, doc) => {
                ResponseHelper.sendResponse(res, 200, doc)
            }) 
        }
    }
}

module.exports = AuthController