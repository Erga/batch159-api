const DB = require('../models/db')
const ObjectID = require('mongodb').ObjectID
const ResponseHelper = require('../helpers/responsehelpers')
const Menu = require('../models/menumodel')
const MiscHelper = require('../helpers/miscHelper')
const db = DB.getConnection()

const MenuControllers = {

    // Membaca Semua Menu
    readAllMenu: (req,res, next) => {
        db.collection('menus').find({}).toArray((err, docs) => {
            let menuss = docs.map((ele) => {
                return  new Menu(ele)
            })
            ResponseHelper.sendResponse(res, 200, docs)
        })
    },

    // Membuat Menu Baru
    createMenu: (req, res, next) => {
        console.log(req.body)
        
        let data = req.body
        data.kode_menu = MiscHelper.createRandomNumber()

        let menuObject = new Menu(data)
        db.collection('menus').insert(menuObject, (err, doc) => {
            ResponseHelper.sendResponse(res, 200, doc)
        })
    },

    // Mengubah Menu
    updateMenu: (req, res, next) => {
        let data = req.body
        data._id = new ObjectID(req.params.menuid)
        let menuObject = new Menu(data)
        db.collection('menus').update(
            {_id: menuObject._id},
            menuObject,
            (err, doc) => {
                ResponseHelper.sendResponse(res, 200, doc)
            })
    },

    // Menghapus Menu
    deleteMenu: (req, res, next) => {
        let menuid = new ObjectID(req.params.menuid)
        db.collection('menus').remove(
            {_id: menuid},
            (err, doc) => {
                ResponseHelper.sendResponse(res, 200, doc)
            })    
    }
}
module.exports = MenuControllers