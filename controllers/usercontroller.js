const DB = require('../models/db')
const ObjectID = require('mongodb').ObjectID
const ResponseHelper = require('../helpers/responsehelpers')
const User = require('../models/usermodel')
const MiscHelper = require('../helpers/miscHelper')
const db = DB.getConnection()
const bcrypt = require('bcrypt')

const UserControllers = {
    hello: (req, res, next) => {
        res.send('hello friend')
    },

    // Membaca Semua User
    readAllHandler: (req,res, next) => {
        console.log(req.userdata)
        db.collection('users').find({}).toArray((err, docs) => {
            let users = docs.map((ele) => {
                return  new User(ele)
            })
            ResponseHelper.sendResponse(res, 200, docs)
        })
    },

    // Membuat User Baru
    createHandler: (req, res, next) => {
        console.log(req.body)
        
        let data = req.body
        data.kode_user = MiscHelper.createRandomNumber()

        // Encrypt Password User
        let salt = bcrypt.genSaltSync(10);
        let hash = bcrypt.hashSync(data.password, salt);
        data.password = hash

        let userObject = new User(data)
        db.collection('users').insert(userObject, (err, doc) => {
            ResponseHelper.sendResponse(res, 200, doc)
        })
    },

    // Mengubah User
    updateHandler: (req, res, next) => {
        let data = req.body
        data._id = new ObjectID(req.userdata._id)
        let userObject = new User(data)
        db.collection('users').update(
            {_id: userObject._id},
            userObject,
            (err, doc) => {
                ResponseHelper.sendResponse(res, 200, doc)
            })
    },

    // Menghapus User
    deleteHandler: (req, res, next) => {
        let userid = new ObjectID(req.params.userid)
        db.collection('users').remove(
            {_id: userid},
            (err, doc) => {
                ResponseHelper.sendResponse(res, 200, doc)
            })    
    }
}
module.exports = UserControllers