// Restify & Database
const restify = require('restify')
const DatabaseConnection = require('./models/db')
const AuthMiddleware = require('./middlewares/auth.middleware')
const corsMiddleware = require('restify-cors-middleware')

console.log('testing')

DatabaseConnection.connect((err, db) => {
    if (err != null) {
        console.log(err)
        process.exit()
    } else {
        console.log('[DATABASE] connected')
        const server = restify.createServer()
        const port = 3000

        // Controllers
        const UserControllers = require('./controllers/usercontroller')
        const TableControllers = require('./controllers/tablecontroller')
        const MenuControllers = require('./controllers/menucontroller')
        const CategoryControllers = require('./controllers/categorycontroller')
        const AuthControllers = require('./controllers/authcontroller')

        server.use(restify.plugins.queryParser());
        server.use(restify.plugins.bodyParser({mapParams: false}));
        const cors = corsMiddleware({
            preflightMaxAge: 5, //Optional
            // origins: ['http://api.myapp.com', 'http://web.myapp.com'],
            allowHeaders: ['Authorization'],
            exposeHeaders: ['API-Token-Expiry']
          })
          server.pre(cors.preflight)
          server.use(cors.actual)

        // Routes (User)
        server.get('/api/users', AuthMiddleware.checktoken, UserControllers.readAllHandler)
        server.post('/api/users', AuthMiddleware.checktoken, UserControllers.createHandler)
        server.put('/api/users', AuthMiddleware.checktoken, UserControllers.updateHandler)
        server.del('/api/users/:userid', UserControllers.deleteHandler)

        // Routes (Table)
        server.get('/api/tables', AuthMiddleware.checktoken, TableControllers.readAllTable)
        server.get('/api/tables/:tableid', AuthMiddleware.checktoken, TableControllers.readOneById)
        server.post('/api/tables', AuthMiddleware.checktoken, TableControllers.createTable)
        server.put('/api/tables/:tableid', AuthMiddleware.checktoken, TableControllers.updateTable)
        server.del('/api/tables/:tableid', AuthMiddleware.checktoken, TableControllers.deleteTable)

        // Routes (Menu)
        server.get('/api/menus', MenuControllers.readAllMenu)
        server.post('/api/menus', MenuControllers.createMenu)
        server.put('/api/menus/:menuid', MenuControllers.updateMenu)
        server.del('/api/menus/:menuid', MenuControllers.deleteMenu)

        // Routes (Category)
        server.get('/api/categorys', CategoryControllers.readAllCategory)
        server.post('/api/categorys', CategoryControllers.createCategory)
        server.put('/api/categorys/:categoryid', CategoryControllers.updateCategory)
        server.del('/api/categorys/:categoryid', CategoryControllers.deleteCategory)

        // Routes (Login & Register)
        server.post('/api/auth/login', AuthControllers.loginHandler)
        server.post('/api/auth/register', AuthControllers.registerHandler)

        server.listen(port, () => {
            console.log('[SERVER] running at port' +port)
        })
    }
})
